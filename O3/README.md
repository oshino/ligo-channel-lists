Observing Run 3
===============

This directory contains those channel lists for the second aLIGO observing run, 
beginning in summer 2016.

O3 Omicron Online Documentation
===============
Line syntax
----
All channel names are followed by their native sampling rate and two strings: 
the first is an indication of safety and the second is an indication of signal fidelity. 

**Safety**

Channels are marked as: safe, unsafe, unsafeabove2kHz, unknown

**Signal fidelity**

Channels are marked as: clean, flat, glitchy, unknown

Where flat indicates the channel is temporarily unplugged or not in use, and 
glitchy means the channel contains a high rate of transient noise artifacts. 

Unknown indicates the state of the channel (safety and/or signal fidelity) is ambiguous or unknown.

h(t)
----
**Summary** 

Triggers produced for GDS-CALIB_STRAIN have a wide frequency range, up to 8 kHz, 
a finely populated sine-Gaussian template basis with 0.2 allowed mismatch, 
and an lower SNR threshold 
of 5. Triggers are only produced when the ifo is running nominally and 
calibration is also in a good state. 

**Parameters**

- Frequency range: 4 - 8192 Hz
- Q range: 3.3155 - 150
- State required to produce triggers: DMT-CALIBRATED:1
- Chunk duration: 124
- Segment duration: 64
- Overlap duration: 4
- Maximum allowed mismatch: 0.2
- SNR threshold: 5

---

Auxiliary channels
---

Auxiliary channels are processed with a less finely space sine-Gaussian tiling
(a maximum allowed mismatch of 0.35) and an SNR threshold of 5.5

Auxiliary channels are grouped by valid frequency range, sampling rate, and 
required state for producing triggers. Omicron parameters for each group are 
designed to maximize the amount of information from auxiliary channel triggers, 
as defined here: https://git.ligo.org/detchar/ligo-channel-lists/blob/master/tools/clf-to-omicron.py.

Omicron triggers are produced for most auxiliary channels with the ifo is 
operating in a nominal state (DMT-GRD_ISC_LOCK_NOMINAL:1) except for ground 
motion monitors and PEM sensors, which always produce triggers. 

Configurations for new python-based Omega scans can be defined in a similar
way: https://git.ligo.org/detchar/ligo-channel-lists/blob/master/tools/clf-to-pyomega.py.
These configurations will have similar properties to those for Omicron, except
that by default they exclude explicitly unsafe channels.

---
O3 safety studies
===============

O3 safety studies are yet to be performed.
